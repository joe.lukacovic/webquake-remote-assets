import * as router from 'aws-lambda-router'
import * as AWS from 'aws-sdk'
import { promisify } from 'util';

export const handler = router.handler({
  proxyIntegration: {
    debug: true,
    routes: [{
      // request-path-pattern with a path variable:
      path: '/api/assets/:directory',
      method: 'GET',
      // we can use the path param 'id' in the action call:
      action: (request, context) => {
        const dir = request.paths.directory
        if (!dir) {
          throw {reason: 'NotFound', message: ''}
        }

        const prefix = dir + '/'
        const s3 = new AWS.S3();
        const params = {
          Bucket: process.env.ASSETS_BUCKET_NAME, 
          Delimiter: '',
          Prefix: prefix
        };

        return promisify(s3.listObjects.bind(s3))(params)
          .then(results => results.Contents
            .filter(item => item.Key !== prefix)
            .map(item => ({
              fileName: item.Key,
              size: item.Size
            })))
      }
    }],
    errorMapping: {
      'NotFound': 404,
      'MyCustomError': 429,
      'ServerError': 500
    }
  }
})